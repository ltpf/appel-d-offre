// @ts-nocheck
import ListAbsences from "./components/business/absences/ListAbsences"
import ListEmploye from "./components/business/employee/ListEmploye"
import ShowAllExpenseReport from "./components/business/expenses/ShowAllExpenseReport"

function Home() {
  return (
    <div className="p-5 flex justify-around gap-x-8">
      <ListEmploye />
      <div className="flex flex-col gap-y-4 justify-between">
        <ShowAllExpenseReport />
        <ListAbsences />
      </div>
    </div>
  )
}

export default Home
