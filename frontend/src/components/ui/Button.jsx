/* eslint-disable react/prop-types */
import clsx from "clsx"

const variants = {
  primary:
    "bg-blue  text-white font-bold",
}

const hovers = {
  none: "",
  hover: "hover:bg-purple",
}

const sizes = {
  medium: "py-2 ml-5 px-4",
}

const borderRadius = {
  none: "",
  medium: "rounded",
}

const Button = (props) => {
  const {
    // eslint-disable-next-line react/prop-types
    variant = "primary",
    size = "medium",
    hover = "hover",
    radius = "medium",
    className,
    ...otherProps
  } = props

  return (
    <button
      className={clsx(
        sizes[size],
        variants[variant],
        hovers[hover],
        borderRadius[radius],
        className
      )}
      {...otherProps}
    />
  )
}

export default Button
