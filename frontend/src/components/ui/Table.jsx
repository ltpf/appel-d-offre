// @ts-nocheck
/* eslint-disable react/prop-types */

import { Link } from "react-router-dom"

function Table({ headersColumnsMapping, data, link = false }) {
  return (
    <div className="overflow-x-auto relative pt-5 rounded">
      <table className="w-full text-sm text-left border-solid border-2 border-black p-2 rounded-lg">
        <thead className="text-xs text-white uppercase bg-blue">
          <tr>
            {Object.keys(headersColumnsMapping).map((header) => (
              <th key={header} scope="col" className="py-3 px-6">
                {header}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((row) => {
            const rowLink = link ? link(row) : null

            return (
              <tr key={row.id} className="border-b even:bg-gray-100 odd:bg-white">
                {Object.values(headersColumnsMapping).map((valueGetter) => {
                  const value = typeof valueGetter === "function" ? valueGetter(row) : row[valueGetter]

                  return (
                    <td key={value} className="py-4 px-6">
                      {rowLink ? <Link to={rowLink}>{value}</Link> : value}
                    </td>
                  )
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}
export default Table
