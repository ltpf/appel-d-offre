// @ts-nocheck
/* eslint-disable react/prop-types */

import { useCallback, useMemo } from "react"

function Pagination({ totalItems, itemsPerPage, currentPage, setCurrentPage }) {
  const totalPages = useMemo(() => Math.ceil(totalItems / itemsPerPage), [totalItems, itemsPerPage])

  const handlePreviousPage = useCallback(
    () => console.log(currentPage) || setCurrentPage((page) => Math.max(page - 1, 0)),
    [setCurrentPage, currentPage]
  )
  const handleNextPage = useCallback(
    () => console.log(currentPage) || setCurrentPage((page) => Math.min(page + 1, totalPages - 1)),
    [setCurrentPage, totalPages, currentPage]
  )

  return (
    <div className="select-none">
      <small className="text-gray-600 cursor-pointer" onClick={handlePreviousPage} disabled={currentPage === 0}>
        Précédent
      </small>
      <span className="border-solid border-2 border-black p-1 m-1">{currentPage + 1}</span>
      <span>...</span>
      <span className="border-solid border-2 border-black p-1 m-1">{totalPages}</span>
      <small
        className="text-gray-600 cursor-pointer"
        onClick={handleNextPage}
        disabled={currentPage === totalPages - 1}
      >
        Suivant
      </small>
    </div>
  )
}
export default Pagination
