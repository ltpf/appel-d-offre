import React from 'react';
import ReactModal from 'react-modal';
import Button from '../Button';

const Modal = ({ isOpen, onClose, title, children, onSubmit }) => {
  return (
    <ReactModal
      isOpen={isOpen}
      onRequestClose={onClose}
      contentLabel={title}
      ariaHideApp={false} // Set to false to prevent a warning about appElement
      className={`modal ${isOpen ? 'blur-bg' : ''} w-full md:w-2/3 lg:w-1/2 xl:w-1/3`}

    >
      <div className="modal-container p-6 bg-white rounded-md shadow-md">
        <div className="flex justify-between items-center mb-4">
          <h2 className="text-2xl font-bold">{title}</h2>
          <button
            onClick={onClose}
            className="text-black-500 hover:text-red-700 focus:outline-none font-extrabold text-2xl"
          >
            <p> X </p>
          </button>
        </div>
        {children}
        <div className='mt-4 flex justify-center'>
          <Button onClick={onSubmit}>Valider</Button>
        </div>
      </div>
    </ReactModal>
  );
};

export default Modal;
