// @ts-nocheck
import "react-toastify/dist/ReactToastify.css"

import { Outlet } from "react-router-dom"

import { ToastContainer } from "react-toastify"

import Header from "./Header"

export default function Layout() {
  return (
    <>
      <Header />
      <main>
        <Outlet />
      </main>
      <ToastContainer />
    </>
  )
}
