// @ts-nocheck
import { useState } from "react"
import clsx from "clsx"

import { Link } from "react-router-dom"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faBars } from "@fortawesome/free-solid-svg-icons"

const BurgerMenu = () => {
  const [isOpen, setIsOpen] = useState(false)

  const toggleMenu = () => {
    setIsOpen(!isOpen)
  }

  return (
    <header>
      {isOpen && (
        <div className="fixed inset-0 bg-black bg-opacity-50 backdrop-blur-sm z-10" onClick={toggleMenu}></div>
      )}

      <div className="flex items-center justify-between bg-white p-2">
        <Link to="/" className="p-2 cursor-pointer bg-black-500"> 
          <img src="https://res.cloudinary.com/dfqxbwfnc/image/upload/v1705518664/Capture_d_e%CC%81cran_2024-01-17_a%CC%80_19.48.15_hwv3di.png" alt="Logo" className="h-10 w-auto" />
        </Link>
        <button className="pr-4" onClick={toggleMenu}>
          <FontAwesomeIcon icon={faBars} size="2x" />
        </button>
        <div
          className={clsx("fixed inset-y-0 right-0 w-64 bg-white shadow-md transition-transform z-10", {
            "translate-x-0": isOpen,
            "translate-x-full": !isOpen,
          })}
        >
          <div className="flex justify-between p-2">
            <h2 className="font-bold text-2xl">Menu</h2>
            <button className="z-20" onClick={toggleMenu}>
              <svg className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
              </svg>
            </button>
          </div>
          <div className="mt-4 px-4 divide-y divide-black text-center">
            <a href="#" className="block p-2 hover:bg-gray-200 hover:opacity-75">
              <Link to="/">ACCUEIL</Link>
            </a>
            <a href="#" className="block p-2 hover:bg-gray-200 hover:opacity-75">
              <Link to="/employes">EMPLOYÉS</Link>
            </a>
            <a href="#" className="block p-2 hover:bg-gray-200 hover:opacity-75">
              <Link to="/absences">ABSENCES</Link>
            </a>
            <a href="#" className="block p-2 hover:bg-gray-200 hover:opacity-75">
              <Link to="/materiel">MATÉRIEL</Link>
            </a>
            <a href="#" className="block p-2 hover:bg-gray-200 hover:opacity-75">
              <Link to="/notes-de-frais">NOTE DE FRAIS</Link>
            </a>
            <a href="#" className="block p-2 hover:bg-gray-200 hover:opacity-75">
              <Link to="/documents">DOCUMENTS</Link>
            </a>
          </div>
        </div>
      </div>
    </header>
  )
}

export default BurgerMenu
