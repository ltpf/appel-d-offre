// @ts-nocheck

import axios from "axios"
import { useEffect, useState } from "react"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faHouse } from "@fortawesome/free-solid-svg-icons"

import showToast from "../../ui/Toast"
import Pagination from "../../ui/Pagination"
import Table from "../../ui/Table"

const headersColumnsMapping = {
  "Name": (row) => `${row.absenteist.lastname} ${row.absenteist.firstname}`,
  "Type": "type",
  "Durée": (row) => {
    const startTime = new Date(row.startTime)
    const endTime = new Date(row.endTime)

    let durationInSeconds = (endTime - startTime) / 1000

    let hours = Math.floor(durationInSeconds / 3600)
    const days = Math.floor(hours / 24)
    hours %= 24

    const durationStringParts = []

    if (days > 0) {
      durationStringParts.push(`${days} jour${days > 1 ? "s" : ""}`)
    }

    if (hours > 0) {
      durationStringParts.push(`${hours} heure${hours > 1 ? "s" : ""}`)
    }

    return durationStringParts.join(" et ") || "Aucune"
  },
  "Date de fin": (row) => {
    const endTime = new Date(row.endTime)

    const dateString = endTime.toLocaleDateString("fr-FR", { dateStyle: "medium" })
    const timeString = endTime.toLocaleTimeString("fr-FR", { timeStyle: "short" })

    return `Le ${dateString} à ${timeString}`
  },
}

const itemsPerPage = 10

export default function ListAbsences() {
  const [absences, setAbsences] = useState([])
  const [currentPage, setCurrentPage] = useState(0)
  const [totalItems, setTotalItems] = useState(0)

  useEffect(() => {
    axios
      .get("http://localhost:3001/absences", {
        params: {
          sort: "startTime:desc",
          startTime__lte: new Date().toISOString(),
          limit: itemsPerPage,
          page: currentPage,
        },
      })
      .then((res) => {
        setTotalItems(res.data.totalItems)
        setAbsences(res.data.results)
      })
      .catch(() => showToast("Erreur durant la récupération des données", "error"))
  }, [currentPage])

  return (
    <div className="bg-white p-5 rounded shadow-md font-bold">
      <div className="flex justify-between">
        <div className="flex p-2">
          <h1 className="text-xl">Absences à venir</h1>
          {/* <EmployeeManager /> */}
        </div>
        <FontAwesomeIcon icon={faHouse} size="2x" className="p-2" />
      </div>
      <Table headersColumnsMapping={headersColumnsMapping} data={absences} link={(row) => `/absences/${row.id}`} />
      {absences.length > 0 ? (
        <div className="mt-3">
          <Pagination
            totalItems={totalItems}
            itemsPerPage={itemsPerPage}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
        </div>
      ) : null}
    </div>
  )
}
