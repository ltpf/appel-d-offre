// @ts-nocheck
import { useState, useEffect } from "react"
import axios from "axios"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faDollarSign } from "@fortawesome/free-solid-svg-icons"
import Table from "../../ui/Table"
import Pagination from "../../ui/Pagination"

const headersColumnsMapping = {
  Nom: "name",
  Type: "type",
  Date: "date",
  TTC: "itCost",
}

const ShowAllExpenseReport = () => {
  const itemsPerPage = 6
  const [currentPage, setCurrentPage] = useState(0)
  const [expenses, setExpenses] = useState([])

  useEffect(() => {
    axios
      .get("http://localhost:3001/api/get_all_expense_reports")
      .then((response) => {
        // Extracting only the required properties from each item
        const modifiedExpenses = response.data.map((item) => ({
          id: item.id,
          name: item.name,
          type: item.type,
          date: item.date,
          itCost: item.itCost,
        }))
        setExpenses(modifiedExpenses)
      })
      .catch((error) => {
        console.error("There was an error!", error)
      })
  }, [])

  // Calcul de l'index de début et de fin pour l'affichage des données
  const startIndex = currentPage * itemsPerPage
  const endIndex = startIndex + itemsPerPage
  const displayedData = expenses.slice(startIndex, endIndex)

  return (
    <div className="bg-white p-5 rounded shadow-md font-bold">
      <div className="flex justify-between">
        <div className="flex p-2">
          <h1 className="text-xl">Notes de frais</h1>
        </div>
        <FontAwesomeIcon icon={faDollarSign} size="2x" className="p-2" />
      </div>
      <Table headersColumnsMapping={headersColumnsMapping} data={displayedData} link={(row) => `/depenses/${row.id}`} />
      <div className="mt-3">
        <Pagination
          totalItems={expenses.length}
          itemsPerPage={itemsPerPage}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
        />
      </div>
    </div>
  )
}

export default ShowAllExpenseReport
