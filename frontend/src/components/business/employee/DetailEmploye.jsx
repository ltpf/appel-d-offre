// @ts-nocheck
// DetailEmployee.jsx
import { useState, useEffect } from "react"
import { useParams } from "react-router-dom"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faUser } from "@fortawesome/free-solid-svg-icons"
import { faUsers } from "@fortawesome/free-solid-svg-icons"
import { faLayerGroup } from "@fortawesome/free-solid-svg-icons"
import { faReceipt } from "@fortawesome/free-solid-svg-icons"
import axios from "axios"
import Button from "../../ui/Button"
import Table from "../../ui/Table"
import Pagination from "../../ui/Pagination"
import clsx from "clsx"

const sections = {
  absences: {
    name: "absences",
    icon: faUsers,
    api: {
      endpoint: "/absences",
      idParam: "createUserId",
      sort: "startTime:desc",
    },
    headersColumnsMapping: {
      "Date de début": "startTime",
      "Date de fin": "endTime",
      "Date d'envoi": "submissionDate",
      "Status": "status",
      "Commentaire": "comment",
    },
    emptyMessage: "Aucune absence pour le moment pour cet employé.",
  },
  resources: {
    name: "ressources",
    icon: faLayerGroup,
    api: {
      endpoint: "/documents",
      idParam: "userId",
      sort: "name:desc",
    },
    headersColumnsMapping: {
      "Nom du document": (row) => row.name,
      "Document émis par": (row) => `${row.uploader.lastname} ${row.uploader.firstname}`,
      "Action": (row) => (
        <>
          Télécharger{" "}
          <a className="underline text-cyan-600" href={`http://localhost:3001${row.path}`} download={row.name}>
            {row.name}
          </a>
        </>
      ),
    },
    emptyMessage: "Aucune ressource pour le moment pour cet employé.",
  },
  expenses: {
    name: "dépenses",
    icon: faReceipt,
    api: {
      endpoint: "/receipts",
      idParam: "userId",
      sort: "itCost:desc",
    },
    headersColumnsMapping: {
      "Nom de la dépense": "name",
      "Date": (row) => {
        const date = new Date(row.date)

        const dateString = date.toLocaleDateString("fr-FR", { dateStyle: "medium" })
        const timeString = date.toLocaleTimeString("fr-FR", { timeStyle: "short" })

        return `Le ${dateString} à ${timeString}`
      },
      "Montant": "itCost  ",
      "Commentaire": "comment",
      "Justificatif": (row) =>
        row.document.path ? (
          <a
            className="underline text-cyan-600"
            href={`http://localhost:3001/${row.document.path}`}
            download={row.name}
          >
            {row.name}
          </a>
        ) : (
          "Aucun"
        ),
    },
    emptyMessage: "Aucune dépense pour le moment pour cet employé.",
  },
}

const itemsPerPage = 10

function DetailEmploye() {
  const { id } = useParams()
  const [employe, setEmploye] = useState(null)
  const [data, setData] = useState([])
  const [currentSection, setCurrentSection] = useState("absences")
  const [currentPage, setCurrentPage] = useState(0)
  const [totalItems, setTotalItems] = useState(0)
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(false)

  useEffect(() => {
    const section = sections[currentSection]

    axios
      .get(`http://localhost:3001${section.api.endpoint}`, {
        params: {
          sort: section.api.sort,
          [`${section.api.idParam}__eq`]: id,
          limit: itemsPerPage,
          page: currentPage,
        },
      })
      .then((res) => {
        setTotalItems(res.data.totalItems)
        setData(res.data.results)
        setLoading(false)
        setError(false)
      })
      .catch(() => {
        setLoading(false)
        setError(false)
      })
  }, [currentSection, id, currentPage])

  useEffect(() => {
    axios
      .get(`http://localhost:3001/api/get_employee/${id}`)
      .then((response) => {
        setEmploye(response.data)
        setLoading(false)
      })
      .catch((error) => {
        console.error("There was an error fetching the employee details:", error)
        setError(error)
        setLoading(false)
      })
  }, [id])

  if (loading) {
    return <div>Chargement...</div>
  }

  if (error) {
    return <div>Erreur de chargement.</div>
  }

  const section = sections[currentSection]

  return (
    <div className="employee-detail">
      {employe ? (
        <>
          <div className="flex justify-evenly w-full mt-9 pt-5">
            <div className="flex">
              <FontAwesomeIcon icon={faUser} size="10x" className="p-2 text-gray-600 bg-gray-200" />
              <div className="ml-5">
                <div className="font-medium text-3xl">
                  <span className="uppercase">{employe.lastname} </span>
                  {employe.firstname}
                </div>
                <div>{employe.poste}</div>
                <div className="font-light">
                  <small>{employe.email}</small> - <small>{employe.phone}</small>
                </div>
              </div>
            </div>
            <div className="flex w-1/3 justify-between flex-col">
              {Object.entries(sections).map(([key, section]) => (
                <Button
                  key={key}
                  className={clsx("w1/3", { "bg-purple": currentSection === key })}
                  onClick={() => {
                    setLoading(true)
                    setCurrentPage(0)
                    setCurrentSection(key)
                  }}
                >
                  Voir les {section.name}
                </Button>
              ))}
            </div>
          </div>
          <div className="w-4/5 mx-auto mt-5 pt-5">
            {
              <div className="bg-white p-5 rounded shadow-md font-bold space-y-4">
                <div className="flex justify-between">
                  <div className="flex p-2">
                    <h1 className="text-xl">Liste des {section.name}</h1>
                    <Button>Ajouter une {section.name.slice(0, -1)}</Button>
                  </div>
                  <FontAwesomeIcon icon={section.icon} size="2x" className="p-2" />
                </div>
                {data.length > 0 ? (
                  <>
                    <Table headersColumnsMapping={section.headersColumnsMapping} data={data} />
                    <Pagination
                      currentPage={currentPage}
                      setCurrentPage={setCurrentPage}
                      itemsPerPage={itemsPerPage}
                      totalItems={totalItems}
                    />
                  </>
                ) : (
                  <p>{section.emptyMessage}</p>
                )}
              </div>
            }
          </div>
        </>
      ) : (
        <div>Aucun employé trouvé.</div>
      )}
    </div>
  )
}

export default DetailEmploye
