/* eslint-disable react/prop-types */
import { useState } from 'react';
import Modal from '../../ui/Modals/Modal';
import axios from 'axios';
import showToast from '../../ui/Toast';

const EmployeeFormModal = ({ isOpen, onClose, onSubmit }) => {
  const [formData, setFormData] = useState({
    firstname: '',
    lastname: '',
    dateOfBirth: '',
    email: '',
    phone: '',
    address: '',
    ville: '',
    codePostale: '',
    poste: ''
  });

  const [errors, setErrors] = useState({
    firstname: '',
    lastname: '',
    dateOfBirth: '',
    email: '',
    phone: '',
    address: '',
    ville: '',
    codePostale: '',
    poste: ''
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
    // Clear the error message when the user starts typing in a field
    setErrors((prevErrors) => ({ ...prevErrors, [name]: '' }));
  };

  const validateForm = () => {
    let isValid = true;
    const newErrors = { ...errors };

    // Check if required fields are empty
    Object.keys(formData).forEach((fieldName) => {
      if (formData[fieldName] === '') {
        newErrors[fieldName] = 'This field is required';
        isValid = false;
      }
    });

    setErrors(newErrors);
    return isValid;
  };

  const handleFormSubmit = async () => {
    try {
      // Validate the form before submitting
      if (!validateForm()) {
        showToast('Please fill in all required fields', 'error');
        return;
      }

      // logic to send the form data to your backend API
      const response = await axios.post('http://localhost:3001/api/new_employee', formData);

      if (response.status >= 200 && response.status < 300) {

        // Call the onSubmit function provided as a prop
        if (typeof onSubmit === 'function') {
          onSubmit(response.data);
        }

        // Clear the form data after successful submission
        setFormData({
          firstname: '',
          lastname: '',
          dateOfBirth: '',
          email: '',
          phone: '',
          address: '',
          ville: '',
          codePostale: '',
          poste: ''
        });

        showToast('Form submitted successfully', 'success');
        onClose();
      } else {
        console.error('Error submitting form data:', response);
        showToast('Error submitting form data', 'error');
      }
    } catch (error) {
      console.error('Error submitting form data:', error);
      showToast('Error submitting form data', 'error');
    }
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      onSubmit={handleFormSubmit}
      contentClassName="w-full md:w-1/2 lg:w-1/3 xl:w-1/4"
    >
      <div className="border-b pb-2 mb-4 border-gray-800">
        <h2 className="text-xl font-semibold">Ajouter une personne</h2>
      </div>
      <form>
        <div className="grid grid-cols-2 gap-4">
          <div className="mb-4">
            <label className="block text-sm font-semibold text-gray-600 mb-1">
              Prénom
            </label>
            <input
              type="text"
              name="firstname"
              value={formData.firstname}
              onChange={handleInputChange}
              className={`w-full border border-gray-300 rounded px-3 py-2 ${errors.firstname ? 'border-red-500' : ''
                }`}
              required

            />
            {errors.firstname && <p className="text-red-500 text-sm">{errors.firstname}</p>}
          </div>

          <div className="mb-4">
            <label className="block text-sm font-semibold text-gray-600 mb-1">
              Nom
            </label>
            <input
              type="text"
              name="lastname"
              value={formData.lastname}
              onChange={handleInputChange}
              className={`w-full border border-gray-300 rounded px-3 py-2 ${errors.lastname ? 'border-red-500' : ''
                }`}
              required
            />
            {errors.firstname && <p className="text-red-500 text-sm">{errors.lastname}</p>}
          </div>

          <div className="mb-4">
            <label className="block text-sm font-semibold text-gray-600 mb-1">
              Date de naissance
            </label>
            <input
              type="date"
              name="dateOfBirth"
              value={formData.dateOfBirth}
              onChange={handleInputChange}
              className={`w-full border border-gray-300 rounded px-3 py-2 ${errors.dateOfBirth ? 'border-red-500' : ''
                }`}
              required
            />
            {errors.firstname && <p className="text-red-500 text-sm">{errors.lastname}</p>}
          </div>
          <div className="mb-4">
            <label className="block text-sm font-semibold text-gray-600 mb-1">
              Genre
            </label>
            <div className="flex items-center space-x-2">
              <label>
                <input
                  type="radio"
                  name="gender"
                  value="male"
                  onChange={handleInputChange}
                  className="mr-1"
                />
                Homme
              </label>
              <label>
                <input
                  type="radio"
                  name="gender"
                  value="female"
                  className="mr-1"
                />
                Femme
              </label>
              <label>
                <input
                  type="radio"
                  name="gender"
                  value="other"
                  className="mr-1"
                />
                Other
              </label>
            </div>
          </div>

          <div className="mb-4">
            <label className="block text-sm font-semibold text-gray-600 mb-1">
              Adresse e-mail
            </label>
            <input
              type="email"
              name="email"
              value={formData.email}
              onChange={handleInputChange}
              className={`w-full border border-gray-300 rounded px-3 py-2 ${errors.email ? 'border-red-500' : ''
                }`}
              required
            />
            {errors.firstname && <p className="text-red-500 text-sm">{errors.email}</p>}
          </div>

          <div className="mb-4">
            <label className="block text-sm font-semibold text-gray-600 mb-1">
              Téléphone
            </label>
            <input
              type="text"
              name="phone"
              value={formData.phone}
              onChange={handleInputChange}
              className={`w-full border border-gray-300 rounded px-3 py-2 ${errors.email ? 'border-red-500' : ''
                }`}
              required
            />
            {errors.firstname && <p className="text-red-500 text-sm">{errors.email}</p>}
          </div>

          <div className="mb-4">
            <label className="block text-sm font-semibold text-gray-600 mb-1">
              Adresse postale
            </label>
            <input
              type="text"
              name="address"
              value={formData.address}
              onChange={handleInputChange}
              className={`w-full border border-gray-300 rounded px-3 py-2 ${errors.address ? 'border-red-500' : ''
                }`}
              required
            />
            {errors.firstname && <p className="text-red-500 text-sm">{errors.address}</p>}
          </div>

          <div className="mb-4">
            <label className="block text-sm font-semibold text-gray-600 mb-1">
              Ville
            </label>
            <input
              type="text"
              name="ville"
              value={formData.ville}
              onChange={handleInputChange}
              className={`w-full border border-gray-300 rounded px-3 py-2 ${errors.ville ? 'border-red-500' : ''
                }`}
              required
            />
            {errors.firstname && <p className="text-red-500 text-sm">{errors.ville}</p>}
          </div>

          <div className="mb-4">
            <label className="block text-sm font-semibold text-gray-600 mb-1">
              Code Postale
            </label>
            <input
              type="text"
              name="codePostale"
              value={formData.codePostale}
              onChange={handleInputChange}
              className={`w-full border border-gray-300 rounded px-3 py-2 ${errors.ville ? 'border-red-500' : ''
                }`}
            />
            {errors.firstname && <p className="text-red-500 text-sm">{errors.ville}</p>}
          </div>
          <div className="mb-4">
            <label className="block text-sm font-semibold text-gray-600 mb-1">
              Poste
            </label>
            <input
              type="text"
              name="poste"
              value={formData.poste}
              onChange={handleInputChange}
              className={`w-full border border-gray-300 rounded px-3 py-2 ${errors.poste ? 'border-red-500' : ''
                }`}
              required
            />
            {errors.firstname && <p className="text-red-500 text-sm">{errors.poste}</p>}
          </div>
        </div>
      </form>
    </Modal>
  );
};

export default EmployeeFormModal;
