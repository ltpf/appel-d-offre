import React, { useState } from 'react';
import EmployeeFormModal from './EmployeeFormModal';
import Button from '../../ui/Button';

const EmployeeManager = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  // Handle submitted data here
  const handleFormSubmit = (submittedData) => {
    // Handle the submitted data as needed
    console.log('Submitted data:', submittedData);
  };

  return (
    <div className='flex justify-center'>
      <Button onClick={openModal}>Ajouter un employé</Button>

      <EmployeeFormModal
        isOpen={isModalOpen}
        onClose={closeModal}
        onSubmit={handleFormSubmit}  // Pass the onSubmit function
      />
    </div>
  );
};

export default EmployeeManager;
