// @ts-nocheck
import { useState, useEffect } from "react"
import axios from "axios"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faUsers } from "@fortawesome/free-solid-svg-icons"
import EmployeeManager from "./EmployeeManager"
import Table from "../../ui/Table"
import Pagination from "../../ui/Pagination"

const headersColumnsMapping = { Nom: "Nom", Prénom: "Prénom", Mail: "Mail", Téléphone: "Téléphone" }

function ListEmploye() {
  const itemsPerPage = 20
  const [currentPage, setCurrentPage] = useState(0)
  const [fullEmployesData, setFullEmployesData] = useState([]) // Données complètes des employés
  const [employes, setEmployes] = useState([]) // Données pour l'affichage

  const loadEmployees = () => {
    axios
      .get("http://localhost:3001/api/get_employee")
      .then((response) => {
        setFullEmployesData(response.data) // Stocker les données complètes
        const filteredData = response.data.map(({ lastname, firstname, email, phone, id }) => ({
          id,
          Nom: lastname,
          Prénom: firstname,
          Mail: email,
          Téléphone: phone,
        }))
        setEmployes(filteredData) // Données pour l'affichage
      })
      .catch((error) => {
        console.error("There was an error!", error)
      })
  }
  useEffect(() => {
    loadEmployees()
  }, [])

  const reloadEmployees = () => {
    loadEmployees()
  }

  const startIndex = currentPage * itemsPerPage
  const endIndex = startIndex + itemsPerPage
  const displayedData = employes.slice(startIndex, endIndex)

  return (
    <div className="bg-white p-5 rounded shadow-md font-bold">
      <div className="flex justify-between">
        <div className="flex p-2">
          <h1 className="text-xl">Liste des employés</h1>
          <EmployeeManager onEmployeeAdded={reloadEmployees} />
        </div>
        <FontAwesomeIcon icon={faUsers} size="2x" className="p-2" />
      </div>
      <Table
        headersColumnsMapping={headersColumnsMapping}
        data={displayedData}
        link={(row) => `/detail-employee/${row.id}`}
      />
      <div className="mt-3">
        <Pagination
          totalItems={employes.length}
          itemsPerPage={itemsPerPage}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
        />
      </div>
    </div>
  )
}

export default ListEmploye
