import "./index.css"

import React from "react"
import ReactDOM from "react-dom/client"
import { BrowserRouter, Routes, Route } from "react-router-dom"
import DetailEmploye from './components/business/employee/DetailEmploye';

import Home from "./Home"
import Layout from "./components/business/Layout"

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route element={<Layout />}>
          <Route element={<Home />} index />
          <Route path="/detail-employee/:id" element={<DetailEmploye />} />
        </Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
)
