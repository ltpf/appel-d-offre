/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        purple: "#850578",
        blue: "#0580d9",
        pink: "#fc458c",
        orange: "#fa961c"
      },
      backdropBlur: ['responsive', 'hover', 'focus']
    },
  },
  plugins: [],
}
