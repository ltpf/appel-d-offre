"use strict"

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("assignments", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      brand: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      state: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      assign_date: {
        allowNull: false,
        type: Sequelize.DataTypes.DATE,
      },
      user_id: {
        allowNull: false,
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
    })
  },

  async down(queryInterface) {
    await queryInterface.dropTable("assignments")
  },
}
