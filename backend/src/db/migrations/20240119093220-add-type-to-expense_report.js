"use strict"

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn("expense_reports", "type", {
      allowNull: false,
      type: Sequelize.DataTypes.STRING
    })
  },

  async down(queryInterface) {
    await queryInterface.removeColumn("expense_reports", "type")
  },
}
