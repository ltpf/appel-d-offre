"use strict"

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("absences", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.DataTypes.INTEGER,
      },
      type: {
        allowNull: false,
        type: Sequelize.DataTypes.ENUM,
        values: ["RTT", "Congé", "Enfant malade", "Arrêt maladie"],
      },
      start_time: {
        allowNull: false,
        type: Sequelize.DataTypes.DATE,
      },
      end_time: {
        allowNull: false,
        type: Sequelize.DataTypes.DATE,
      },
      submission_date: Sequelize.DataTypes.DATE,
      status: {
        allowNull: false,
        type: Sequelize.DataTypes.ENUM,
        values: ["Accepté", "Refusé", "En attente"],
        defaultValue: "En attente",
      },
      comment: {
        allowNull: false,
        type: Sequelize.DataTypes.TEXT,
      },
      create_user_id: {
        allowNull: false,
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
      user_id: {
        allowNull: false,
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
      document_id: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: "documents",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
    })
  },
  async down(queryInterface) {
    await queryInterface.dropTable("absences")
    await queryInterface.sequelize.query(
      "DROP TYPE IF EXISTS enum_absences_type",
    )
    await queryInterface.sequelize.query(
      "DROP TYPE IF EXISTS enum_absences_status",
    )
  },
}
