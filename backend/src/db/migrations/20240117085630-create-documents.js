"use strict"

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("documents", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      path: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      type: {
        allowNull: false,
        type: Sequelize.DataTypes.ENUM,
        values: ["Justificatif d'absence", "Reçu"],
      },
      upload_user_id: {
        allowNull: false,
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
      user_id: {
        allowNull: false,
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
    })
  },
  async down(queryInterface) {
    await queryInterface.dropTable("documents")
    await queryInterface.sequelize.query(
      "DROP TYPE IF EXISTS enum_documents_type",
    )
  },
}
