"use strict"

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("expense_reports", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      date: {
        allowNull: false,
        type: Sequelize.DataTypes.DATEONLY,
      },
      it_cost: {
        allowNull: false,
        type: Sequelize.DataTypes.BIGINT,
      },
      df_cost: {
        allowNull: false,
        type: Sequelize.DataTypes.BIGINT,
      },
      comment: {
        allowNull: false,
        type: Sequelize.DataTypes.TEXT,
      },
      document_id: {
        allowNull: false,
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: "documents",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
      user_id: {
        allowNull: false,
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: "users",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
    })
  },

  async down(queryInterface) {
    await queryInterface.dropTable("expense_reports")
  },
}
