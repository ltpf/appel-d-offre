"use strict"

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("users", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.DataTypes.INTEGER,
      },
      firstname: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      lastname: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      birthdate: {
        allowNull: false,
        type: Sequelize.DataTypes.DATEONLY,
      },
      address: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      city: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      zipcode: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      is_admin: {
        allowNull: false,
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: false,
      },
      email: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      phone: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
      poste: {
        allowNull: false,
        type: Sequelize.DataTypes.STRING,
      },
    })
  },

  async down(queryInterface) {
    await queryInterface.dropTable("users")
  },
}
