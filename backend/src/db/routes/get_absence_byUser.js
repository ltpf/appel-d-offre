const express = require('express');
const User = require('../models/user');
const Absence = require('../models/absence');
const router = express.Router();

router.get('/by_user/:userId', async (req, res) => {
  try {
    const userId = req.params.userId;

    
    const user = await User.findByPk(userId);
    if (!user) {
      return res.status(404).send('User not found');
    } 

    
    const absences = await Absence.findAll({
      where: { userId: userId } 
    });

    res.json(absences);
  } catch (error) {
    console.error('Error fetching absences:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
