const express = require('express');
const ExpenseReport  = require('../models/expense_report');
const Document = require('../models/document');
const User = require('../models/user');
const router = express.Router();

// Route pour obtenir les rapports de dépenses d'un employé spécifique
router.get('/by_user/:userId', async (req, res) => {
  try {
    const userId = req.params.userId;
    
    // Vérifiez d'abord si l'utilisateur existe
    const user = await User.findByPk(userId);
    if (!user) {
      return res.status(404).send('User not found');
    }

    // Récupérer les rapports de dépenses associés à cet utilisateur
    const expenseReports = await ExpenseReport.findAll({
      where: { userId: userId },
      include: [{
        model: Document,
        as: 'document' // Remplacez par l'alias correct si vous en avez un
      }]
    });

    res.json(expenseReports);
  } catch (error) {
    console.error('Error fetching expense reports:', error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
