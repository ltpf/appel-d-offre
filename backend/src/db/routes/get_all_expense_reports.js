// routes/employes.js
const express = require('express');
const router = express.Router();
const expenseReport = require('../models/expense_report');

router.get('/', async (req, res) => {
  try {
    const allExpenses = await expenseReport.findAll({})

    res.json(allExpenses);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
  }
});

module.exports = router;