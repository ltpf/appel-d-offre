const express = require('express');
const router = express.Router();
const User = require("../models/user")

router.post('/', async (req, res) => {
  try {
    const { firstname, lastname, dateOfBirth, email, phone, address, ville, codePostale, poste } = req.body;

    // Create a new user in the database
    const newUser = await User.create({
      firstname,
      lastname,
      birthdate: dateOfBirth,
      email,
      phone,
      address,
      city: ville,
      zipcode: codePostale,
      poste
    });

    res.status(201).json(newUser);
  } catch (error) {
    console.error('Error creating employee:', error);
    console.log('Full error object:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

module.exports = router;
