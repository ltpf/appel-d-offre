// routes/employes.js
const express = require("express")
const router = express.Router()
const User = require("../models/user")

router.get("/", async (req, res) => {
  try {
    const employes = await User.findAll({})

    res.json(employes)
  } catch (err) {
    console.error(err.message)
    res.status(500).send("Server error")
  }
})

router.get("/:id", async (req, res) => {
  try {
    const employe = await User.findByPk(req.params.id)
    res.json(employe)
  } catch (err) {
    console.error(err.message)
    res.status(500).send("Server error")
  }
})

module.exports = router
