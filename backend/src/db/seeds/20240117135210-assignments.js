"use strict"

const { faker } = require("@faker-js/faker/locale/fr")

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface) {
    const userIds = await queryInterface.sequelize
      .query("SELECT DISTINCT id FROM users")
      .then(([rawResults]) => rawResults.map((rawResult) => rawResult.id))

    await queryInterface.bulkInsert(
      "assignments",
      faker.helpers.uniqueArray(
        () => ({
          name: faker.commerce.productName(),
          brand: faker.commerce.department(),
          state: faker.helpers.arrayElement([
            "Neuf",
            "Légèrement abimé",
            "Usé",
          ]),
          assign_date: faker.date.recent({ days: 40 }),
          user_id: faker.helpers.arrayElement(userIds),
        }),
        100,
      ),
    )
  },
  async down(queryInterface) {
    await queryInterface.bulkDelete("assignments", null, {})
  },
}
