"use strict"

const { faker } = require("@faker-js/faker/locale/fr")

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface) {
    await queryInterface.bulkInsert(
      "users",
      faker.helpers.uniqueArray(
        () => ({
          firstname: faker.person.firstName(),
          lastname: faker.person.lastName(),
          birthdate: faker.date.birthdate({ mode: "age", min: "18", max: 79 }),
          address: faker.location.streetAddress(),
          city: faker.location.city(),
          zipcode: faker.location.zipCode(),
          is_admin: faker.datatype.boolean(),
          email: faker.internet.email(),
          phone: faker.helpers.fromRegExp(
            "06 [0-9]{2} [0-9]{2} [0-9]{2} [0-9]{2}",
          ),
          poste: faker.person.jobType(),
        }),
        100,
      ),
    )
  },
  async down(queryInterface) {
    await queryInterface.bulkDelete("users", null, {})
  },
}
