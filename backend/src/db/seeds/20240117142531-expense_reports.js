"use strict"

const { faker } = require("@faker-js/faker/locale/fr")

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface) {
    const userIds = await queryInterface.sequelize
      .query("SELECT DISTINCT id FROM users")
      .then(([rawResults]) => rawResults.map((rawResult) => rawResult.id))
    const documentIds = await queryInterface.sequelize
      .query("SELECT DISTINCT id FROM documents")
      .then(([rawResults]) => rawResults.map((rawResult) => rawResult.id))

    await queryInterface.bulkInsert(
      "expense_reports",
      faker.helpers.uniqueArray(
        () => ({
          name: faker.lorem.sentence(),
          date: faker.date.past(),
          df_cost: faker.number.int({ min: 10, max: 30 }),
          it_cost: faker.number.int({ min: 30, max: 200 }),
          comment: faker.lorem.paragraph(),
          user_id: faker.helpers.arrayElement(userIds),
          type: faker.lorem.word(),
          document_id: faker.helpers.arrayElement(documentIds),
        }),
        100,
      ),
    )
  },
  async down(queryInterface) {
    await queryInterface.bulkDelete("expense_reports", null, {})
  },
}
