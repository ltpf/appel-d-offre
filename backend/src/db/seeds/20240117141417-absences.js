"use strict"

const { faker } = require("@faker-js/faker/locale/fr")

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface) {
    const userIds = await queryInterface.sequelize
      .query("SELECT DISTINCT id FROM users")
      .then(([rawResults]) => rawResults.map((rawResult) => rawResult.id))
    const documentIds = await queryInterface.sequelize
      .query("SELECT DISTINCT id FROM documents")
      .then(([rawResults]) => rawResults.map((rawResult) => rawResult.id))

    await queryInterface.bulkInsert(
      "absences",
      faker.helpers.uniqueArray(
        () => ({
          type: faker.helpers.arrayElement([
            "RTT",
            "Congé",
            "Enfant malade",
            "Arrêt maladie",
          ]),
          start_time: faker.date.recent(),
          end_time: faker.date.soon(),
          submission_date: faker.date.recent(),
          status: faker.helpers.arrayElement([
            "Accepté",
            "Refusé",
            "En attente",
          ]),
          comment: faker.lorem.paragraph(),
          create_user_id: faker.helpers.arrayElement(userIds),
          user_id: faker.helpers.arrayElement(userIds),
          document_id: faker.helpers.arrayElement(documentIds),
        }),
        100,
      ),
    )
  },
  async down(queryInterface) {
    await queryInterface.bulkDelete("absences", null, {})
  },
}
