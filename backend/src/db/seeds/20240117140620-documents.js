"use strict"

const { faker } = require("@faker-js/faker/locale/fr")

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface) {
    const userIds = await queryInterface.sequelize
      .query("SELECT DISTINCT id FROM users")
      .then(([rawResults]) => rawResults.map((rawResult) => rawResult.id))

    await queryInterface.bulkInsert(
      "documents",
      faker.helpers.uniqueArray(
        () => ({
          name: faker.lorem.sentence(),
          path: faker.system.filePath(),
          type: faker.helpers.arrayElement(["Justificatif d'absence", "Reçu"]),
          upload_user_id: faker.helpers.arrayElement(userIds),
          user_id: faker.helpers.arrayElement(userIds),
        }),
        100,
      ),
    )
  },
  async down(queryInterface) {
    await queryInterface.bulkDelete("documents", null, {})
  },
}
