const { DataTypes } = require("sequelize")
const sequelize = require("../sequelize")

module.exports = sequelize.define(
  "document",
  {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    path: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    type: {
      allowNull: false,
      type: DataTypes.ENUM,
      values: ["Justificatif d'absence", "Reçu"],
    },
  },
  {
    tableName: "documents",
  },
)
