const { DataTypes } = require("sequelize")
const sequelize = require("../sequelize")

module.exports = sequelize.define(
  "expenseReport",
  {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    date: {
      allowNull: false,
      type: DataTypes.DATEONLY,
      validate: {
        isBefore: new Date().toISOString().split("T")[0],
      },
    },
    itCost: {
      allowNull: false,
      type: DataTypes.BIGINT,
    },
    dfCost: {
      allowNull: false,
      type: DataTypes.BIGINT,
    },
    type: {
      allowNull: false,
      type: DataTypes.STRING
    },
    comment: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
  },
  {
    tableName: "expense_reports",
    validate: {
      isItCostSuperiorToDfCost() {
        // @ts-ignore
        if (this.it_cost < this.df_cost) {
          throw new Error(
            "Price with taxes included should be superior to the price without taxes",
          )
        }
      },
    },
  },
)
