const Absence = require("./absence")
const Assignment = require("./assignment")
const Document = require("./document")
const ExpenseReport = require("./expense_report")
const User = require("./user")

User.hasMany(Absence, {
  foreignKey: {
    allowNull: false,
    field: "create_user_id",
    name: "createUserId",
  },
})
User.hasMany(Absence, {
  foreignKey: {
    allowNull: false,
    field: "user_id",
    name: "userId",
  },
})
User.hasMany(Assignment)
User.hasMany(ExpenseReport)
User.hasMany(Document, {
  as: "uploadedDocuments",
  foreignKey: {
    allowNull: false,
    field: "upload_user_id",
    name: "uploadUserId",
  },
})
User.hasMany(Document, {
  as: "ownedDocuments",
  foreignKey: {
    allowNull: false,
    field: "user_id",
    name: "userId",
  },
})

Absence.belongsTo(User, {
  as: "absenteist",
  foreignKey: {
    allowNull: false,
    field: "user_id",
    name: "userId",
  },
})
Absence.belongsTo(User, {
  as: "creator",
  foreignKey: {
    allowNull: false,
    field: "create_user_id",
    name: "createUserId",
  },
})
Absence.belongsTo(Document)

Assignment.belongsTo(User)

ExpenseReport.belongsTo(User)
ExpenseReport.belongsTo(Document)

Document.hasOne(ExpenseReport)
Document.hasOne(Absence)
Document.belongsTo(User, {
  as: "uploader",
  foreignKey: {
    allowNull: false,
    field: "upload_user_id",
    name: "uploadUserId",
  },
})
Document.belongsTo(User, {
  as: "owner",
  foreignKey: {
    allowNull: false,
    field: "user_id",
    name: "userId",
  },
})
