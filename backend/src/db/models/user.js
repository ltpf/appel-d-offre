const { DataTypes } = require("sequelize")
const sequelize = require("../sequelize")

module.exports = sequelize.define(
  "user",
  {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    firstname: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    lastname: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    birthdate: {
      allowNull: false,
      type: DataTypes.DATEONLY,
      validate: {
         isBefore: {
            args: [new Date().toISOString().split("T")[0]],
            msg: "La date de naissance doit être antérieure à la date d'aujourd'hui."
          }
      },
    },
    address: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    city: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    zipcode: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    isAdmin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    phone: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    poste: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  },
  {
    tableName: "users",
  },
)
