const { DataTypes } = require("sequelize")
const sequelize = require("../sequelize")

module.exports = sequelize.define(
  "assignment",
  {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    brand: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    state: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    assignDate: {
      allowNull: false,
      type: DataTypes.DATE,
      validate: {
        isBefore: new Date().toISOString().split("T")[0],
      },
    },
  },
  {
    tableName: "assignments",
  },
)
