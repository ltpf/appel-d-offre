const { DataTypes } = require("sequelize")
const sequelize = require("../sequelize")

module.exports = sequelize.define(
  "absence",
  {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.INTEGER,
    },
    type: {
      allowNull: false,
      type: DataTypes.ENUM,
      values: ["RTT", "Congé", "Enfant malade", "Arrêt maladie"],
    },
    startTime: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    endTime: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    submissionDate: DataTypes.DATE,
    status: {
      allowNull: false,
      type: DataTypes.ENUM,
      values: ["Accepté", "Refusé", "En attente"],
      defaultValue: "En attente",
    },
    comment: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
  },
  {
    tableName: "absences",
  },
)
