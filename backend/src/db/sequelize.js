const { Sequelize } = require("sequelize")

const sequelizeConfig = require("./config")

const env = process.env.NODE_ENV ?? "development"

module.exports = new Sequelize({
  ...sequelizeConfig[env],
  define: {
    timestamps: false,
    underscored: true,
  },
  logging: env === "production" ? false : console.log,
})
