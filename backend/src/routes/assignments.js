const { crud } = require("../helpers/crud")
const Assignment = require("../db/models/assignment")

module.exports = crud(Assignment)
