const { InvalidArgumentError } = require("../errors")

const { crud } = require("../helpers/crud")

const User = require("../db/models/user")
const Absence = require("../db/models/absence")
const Document = require("../db/models/document")

const userRoutes = crud(User)

userRoutes.get("/:userId/absences", async (req, res) => {
  // @ts-ignore>
  const page = parseInt(req.query.page ?? "0")
  // @ts-ignore>
  const limit = parseInt(req.query.limit ?? "20")

  if (isNaN(page) || isNaN(limit)) {
    throw new InvalidArgumentError(["Page and limit params should be numbers"])
  }

  const absences = await Absence.findAll({
    // @ts-ignore
    where: req.params,
    limit,
    offset: page > 0 ? page * limit - 1 : 0,
  })

  res.send(absences)
})

userRoutes.get("/:userId/documents", async (req, res) => {
  // @ts-ignore>
  const page = parseInt(req.query.page ?? "0")
  // @ts-ignore>
  const limit = parseInt(req.query.limit ?? "20")

  if (isNaN(page) || isNaN(limit)) {
    throw new InvalidArgumentError(["Page and limit params should be numbers"])
  }

  const absences = await Document.findAll({
    // @ts-ignore
    where: req.params,
    limit,
    offset: page > 0 ? page * limit - 1 : 0,
  })

  res.send(absences)
})

userRoutes.get("/:userId/absences", async (req, res) => {
  // @ts-ignore>
  const page = parseInt(req.query.page ?? "0")
  // @ts-ignore>
  const limit = parseInt(req.query.limit ?? "20")

  if (isNaN(page) || isNaN(limit)) {
    throw new InvalidArgumentError(["Page and limit params should be numbers"])
  }

  const absences = await Absence.findAll({
    // @ts-ignore
    where: req.params,
    limit,
    offset: page > 0 ? page * limit - 1 : 0,
  })

  res.send(absences)
})

module.exports = userRoutes
