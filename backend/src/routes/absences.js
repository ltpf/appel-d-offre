const { crud } = require("../helpers/crud")
const multerStorage = require("../helpers/multer-storage")

const Absence = require("../db/models/absence")
const Document = require("../db/models/document")

module.exports = crud(Absence, {
  files: {
    storage: multerStorage,
  },
  endpoints: {
    getAll: {
      include: [Document, Absence.associations.absenteist],
    },
    getOne: {
      include: [Document, Absence.associations.absenteist],
    },
    createOne: {
      file: {
        field: "document",
        /**
         * @param {import("express").Request} req
         * @param {Express.Multer.File} file
         */
        processor: async (req, file) => {
          const document = await Document.create({
            name: file.originalname,
            path: `/${file.path.replace("\\", "/")}`,
            type: "Justificatif d'absence",
            uploadUserId: req.body.createUserId,
            userId: req.body.userId,
          })

          // @ts-ignore
          req.body.documentId = document.id
        },
      },
    },
  },
})
