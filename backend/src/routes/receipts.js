const { crud } = require("../helpers/crud")
const multerStorage = require("../helpers/multer-storage")

const ExpenseReport = require("../db/models/expense_report")
const Document = require("../db/models/document")

module.exports = crud(ExpenseReport, {
  files: {
    storage: multerStorage,
  },
  endpoints: {
    getAll: {
      include: Document,
    },
    getOne: {
      include: Document,
    },
    createOne: {
      file: {
        field: "document",
        /**
         * @param {import("express").Request} req
         * @param {Express.Multer.File} file
         */
        processor: async (req, file) => {
          const document = await Document.create({
            name: file.originalname,
            path: `/${file.path.replace("\\", "/")}`,
            type: "Justificatif d'absence",
            uploadUserId: req.body.createUserId,
            userId: req.body.createUserId,
          })

          // @ts-ignore
          req.body.documentId = document.id
        },
      },
    },
  },
})
