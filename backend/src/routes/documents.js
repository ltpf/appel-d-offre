const { crud } = require("../helpers/crud")
const multerStorage = require("../helpers/multer-storage")

const Document = require("../db/models/document")

const fileOptions = {
  field: "file",
  /**
   * @param {import("express").Request} req
   * @param {Express.Multer.File} file
   */
  processor: (req, file) => {
    req.body.name = file.originalname
    req.body.path = `/${file.path.replace("\\", "/")}`
  },
}

module.exports = crud(Document, {
  files: {
    storage: multerStorage,
  },
  endpoints: {
    getAll: { include: Document.associations.uploader },
    getOne: { include: Document.associations.uploader },
    createOne: {
      file: fileOptions,
    },
    updateOne: {
      file: fileOptions,
    },
  },
})
