const path = require("node:path")
const fs = require("node:fs")

const express = require("express")
const cors = require("cors")

require("express-async-errors")
require("./db/models/init")

const handleError = require("./middlewares/handleError")

const usersRoutes = require("./routes/users")
const absencesRoutes = require("./routes/absences")
const assignmentsRoutes = require("./routes/assignments")
const documentsRoutes = require("./routes/documents")
const receiptsRoutes = require("./routes/receipts")

const getEmployeeRoute = require("./db/routes/get_employee")
const createEmployeeRoute = require("./db/routes/create_employee")
const getExpenseReportsRoute = require("./db/routes/get_expense_report_byUser.js")
const getAbsenceRoute = require("./db/routes/get_absence_byUser.js")
const getAllExpensesReportRoute = require("./db/routes/get_all_expense_reports.js")

const app = express()

app.use(cors())
app.use(express.json())

app.use("/users", usersRoutes)
app.use("/absences", absencesRoutes)
app.use("/assignments", assignmentsRoutes)
app.use("/documents", documentsRoutes)
app.use("/receipts", receiptsRoutes)

app.use("/uploads", (req, res) => {
  const filePath = path.resolve(__dirname, "..", "uploads", req.path.slice(1))

  if (!fs.existsSync(filePath)) {
    res.sendStatus(404)

    return
  }

  res.download(filePath)
})

const port = 3001;

app.use('/api/get_employee', getEmployeeRoute);
app.use('/api/new_employee', createEmployeeRoute);
app.use('/api/get_expense_reports', getExpenseReportsRoute);
app.use('/api/get_all_expense_reports', getAllExpensesReportRoute);
app.use('/api/get_absence_byuser', getAbsenceRoute);

// Middleware de gestion des erreurs
app.use(handleError);


app.listen(port, () => {
  console.log(`Server running on port ${port}`)
})