const { AppError } = require("../errors")

// eslint-disable-next-line no-unused-vars
const handleError = (err, req, res, next) => {
  console.log(err)

  if (err instanceof AppError) {
    res.status(err.httpCode).send({
      error: err.errors,
      errorCode: err.errorCode,
    })

    return
  }

  res
    .status(500)
    .send({ error: ["Oops. Something went wrong."], errorCode: "error" })
}

module.exports = handleError
