const { InvalidArgumentError } = require("../errors")

/**
 *
 * @param {NonNullable<import("express-serve-static-core").Query[string]>} sort
 * @param {string[] | undefined} [sortableFields]
 * @returns {[string, string][]}
 */
exports.generateSort = function generateSort(sort, sortableFields) {
  if (!Array.isArray(sort) && typeof sort !== "string") {
    throw new InvalidArgumentError([
      "Sort param should be either a comma separated list of fields or a list of fields",
    ])
  }

  const order = {}

  let sortItems = typeof sort === "string" ? sort.split(",") : sort

  sortItems.forEach((sortItem) => {
    if (typeof sortItem !== "string") {
      throw new InvalidArgumentError([
        "Sort param should be either a comma separated list of fields or a list of fields",
      ])
    }

    const [field, dir] = sortItem.includes(":")
      ? sortItem.split(":", 2)
      : [sortItem, "ASC"]

    if (sortableFields && !sortableFields.includes(field)) {
      return
    }

    order[field] = dir
  })

  return Object.entries(order)
}
