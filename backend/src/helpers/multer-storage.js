const { randomBytes } = require("node:crypto")
const { extname } = require("node:path")

const { diskStorage } = require("multer")

module.exports = diskStorage({
  destination(req, file, cb) {
    cb(null, "uploads/")
  },
  filename(req, file, cb) {
    cb(null, randomBytes(20).toString("hex") + extname(file.originalname))
  },
})
