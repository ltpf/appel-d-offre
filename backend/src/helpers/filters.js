const { Op } = require("sequelize")

const allowedFilters = [
  "eq",
  "ne",
  "lte",
  "lt",
  "gt",
  "gte",
  "in",
  "notIn",
  "between",
  "notBetween",
  "startsWith",
  "endsWith",
  "like",
  "notLike",
  "substring",
]

/**
 * @param {import("express-serve-static-core").Query} query
 * @param {string[]} [filterableFields]
 * @returns {import("sequelize").WhereOptions}
 */
exports.generateFilter = function generateFilter(query, filterableFields) {
  const whereClause = Object.create(null)

  for (const [key, value] of Object.entries(query)) {
    if (key.match(/__/g)?.length !== 1) {
      continue
    }

    const [attribute, operator] = key.split("__")

    if (
      !allowedFilters.includes(operator) ||
      (filterableFields && !filterableFields.includes(attribute))
    ) {
      continue
    }

    whereClause[attribute] ??= {}

    whereClause[attribute][Op[operator]] = value
  }

  return whereClause
}