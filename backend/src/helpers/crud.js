const { Router } = require("express")
const multer = require("multer")

const { DatabaseError, ValidationError } = require("sequelize")

const { NotFoundError, AppError, InvalidArgumentError } = require("../errors")

const { generateFilter } = require("./filters")
const { generateSort } = require("./sort")

/**
 *
 * @param {import("sequelize").ModelStatic<import("sequelize").Model>} model
 * @param {string[] | undefined} fields
 * @param {string[] | undefined} filterableFields
 * @param {string[] | undefined} sortableFields
 * @param {import("sequelize").FindOptions["include"]} include
 * @returns {import("express").RequestHandler}
 */
function getAll(model, fields, filterableFields, sortableFields, include) {
  return async (req, res) => {
    // @ts-ignore>
    const page = parseInt(req.query.page ?? "0")
    // @ts-ignore>
    const limit = parseInt(req.query.limit ?? "20")

    if (isNaN(page) || isNaN(limit)) {
      throw new InvalidArgumentError([
        "Page and limit params should be numbers",
      ])
    }

    try {
      const { count, rows } = await model.findAndCountAll({
        include,
        attributes: fields,
        limit,
        offset: page > 0 ? page * limit - 1 : 0,
        where: generateFilter(req.query, filterableFields),
        order: req.query.sort
          ? generateSort(req.query.sort, sortableFields)
          : [],
      })

      res.send({
        page,
        itemsPerPage: limit,
        totalItems: count,
        results: rows,
      })
    } catch (e) {
      if (e instanceof DatabaseError && e.message.includes("type")) {
        throw new InvalidArgumentError([e.message])
      }

      throw e
    }
  }
}

/**
 *
 * @param {import("sequelize").ModelStatic<import("sequelize").Model>} model
 * @param {string} param
 * @param {string[] | undefined} fields
 * @param {import("sequelize").FindOptions["include"]} include
 * @returns {import("express").RequestHandler}
 */
function getOne(model, param, fields, include) {
  return async (req, res) => {
    const row = await model.findByPk(req.params[param], {
      include,
      attributes: fields,
    })

    if (!row) {
      throw new NotFoundError()
    }

    res.send(row)
  }
}

/**
 *
 * @param {import("sequelize").ModelStatic<import("sequelize").Model>} model
 * @returns {import("express").RequestHandler}
 */
function createOne(model) {
  return async (req, res) => {
    try {
      const row = await model.create(req.body, {
        fields: Object.keys(model.getAttributes()).filter(
          (attribute) => !model.primaryKeyAttributes.includes(attribute),
        ),
      })

      res.status(201).send(row)
    } catch (e) {
      if (e instanceof DatabaseError) {
        throw new AppError([e.message])
      } else if (e instanceof ValidationError) {
        throw new InvalidArgumentError(e.errors.map((error) => error.message))
      } else if (e instanceof Error) {
        throw new AppError([e.message])
      } else {
        throw new AppError([e.toString()])
      }
    }
  }
}

/**
 *
 * @param {import("sequelize").ModelStatic<import("sequelize").Model>} model
 * @param {string} param
 * @returns {import("express").RequestHandler}
 */
function updateOne(model, param) {
  return async (req, res) => {
    const row = await model.findByPk(req.params[param])

    if (!row) {
      throw new NotFoundError()
    }

    try {
      await row.update(req.body, {
        fields: Object.keys(model.getAttributes()).filter(
          (attribute) => !model.primaryKeyAttributes.includes(attribute),
        ),
      })
    } catch (e) {
      if (e instanceof DatabaseError) {
        throw new AppError([e.message])
      } else if (e instanceof ValidationError) {
        throw new InvalidArgumentError(e.errors.map((error) => error.message))
      } else if (e instanceof Error) {
        throw new AppError([e.message])
      } else {
        throw new AppError([e.toString()])
      }
    }

    res.send(row)
  }
}

/**
 *
 * @param {import("sequelize").ModelStatic<import("sequelize").Model>} model
 * @param {string} param
 * @returns {import("express").RequestHandler}
 */
function deleteOne(model, param) {
  return async (req, res) => {
    const row = await model.findByPk(req.params[param])

    if (!row) {
      throw new NotFoundError()
    }

    try {
      await row.destroy()
    } catch (e) {
      if (e instanceof DatabaseError) {
        throw new AppError([e.message])
      } else if (e instanceof Error) {
        throw new AppError([e.message])
      } else {
        throw new AppError([e.toString()])
      }
    }

    res.status(204).send(row)
  }
}

/**
 *
 * @returns {import("express").RequestHandler}
 */
function processFile(fileOptions) {
  return async (req, res, next) => {
    if (!req.file) {
      if (fileOptions.required ?? false) {
        throw new InvalidArgumentError([
          `File '${fileOptions.field}' is required`,
        ])
      }

      next()
    }

    await fileOptions.processor(req, req.file)

    next()
  }
}

/**
 *
 * @param {import("sequelize").ModelStatic<import("sequelize").Model>} model
 * @returns {import("express").Router}
 */
function crud(model, options = {}) {
  const routerOptions = {}

  if (
    "mergeParams" in options &&
    (typeof options.mergeParams === "boolean" ||
      options.mergeParams === undefined)
  ) {
    routerOptions.mergeParams = options.mergeParams
  }

  const router = Router(routerOptions)
  /**
   * @type {import("multer").Multer | null}
   */
  let fileHandler = null

  let basePath = ""

  if (
    options.prefix &&
    options.prefix?.startsWith("/") &&
    options.prefix?.length > 1
  ) {
    basePath = options.prefix.endsWith("/")
      ? options.prefix.slice(0, -1)
      : options.prefix
  }

  const paramName = `${model.name}${model.primaryKeyAttribute.charAt(0).toUpperCase()}${model.primaryKeyAttribute.slice(1)}`
  const idParamRoute = `${basePath}/:${paramName}`

  if ("files" in options) {
    // @ts-ignore
    if (!options.files.dest && !options.files.storage) {
      throw new Error("`files` option should specify a destination or storage")
    }

    // @ts-ignore
    fileHandler = multer(options.files)
  }

  if (options.endpoints?.getAll ?? true) {
    router.get(
      `${basePath}/`,
      getAll(
        model,
        options.endpoints?.getAll?.fields,
        options.endpoints?.getAll?.filterableFields,
        options.endpoints?.getAll?.sortableFields,
        options.endpoints?.getAll?.include,
      ),
    )
  }

  if (options.endpoints?.getOne ?? true) {
    router.get(
      idParamRoute,
      getOne(
        model,
        paramName,
        options.endpoints?.getOne?.fields,
        options.endpoints?.getOne?.include,
      ),
    )
  }
  if (options.endpoints?.createOne ?? true) {
    const middlewares = []

    if (fileHandler && options.endpoints?.createOne?.file != undefined) {
      const fileOptions = options.endpoints.createOne.file

      if ("field" in fileOptions && "processor" in fileOptions) {
        middlewares.push(
          fileHandler.single(fileOptions.field),
          processFile(fileOptions),
        )
      }
    }

    router.post(`${basePath}/`, ...middlewares, createOne(model))
  }
  if (options.endpoints?.updateOne ?? true) {
    const middlewares = []

    if (fileHandler && options.endpoints?.updateOne?.file != undefined) {
      const fileOptions = options.endpoints.updateOne.file

      if ("field" in fileOptions && "processor" in fileOptions) {
        middlewares.push(
          fileHandler.single(fileOptions.field),
          processFile(fileOptions),
        )
      }
    }

    router.patch(idParamRoute, updateOne(model, paramName))
  }
  if (options.endpoints?.deleteOne ?? true) {
    router.delete(idParamRoute, deleteOne(model, paramName))
  }

  return router
}

module.exports = {
  getAll,
  getOne,
  createOne,
  updateOne,
  deleteOne,
  crud,
}
